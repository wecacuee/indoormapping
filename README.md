Repository for course project CSE741 and Big Data indoor mapping.

This is an Hadoop based Content based image retreival system. We have two
hadoop mapreduce tasks:

  1. Convert the images to SIFT descriptors based database that is saved in the
  form of binary files as Hadoop specific Sequence File format.
  2. The second task distributes the query image to all hadoop nodes and
     computes the euclidean distance of SIFT descriptors to all the images in
     the database. The reducer then chooses the file with minimum distance and
     writes the image path to output.
