package org.ub.indoormapping;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.SequenceFileInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.highgui.Highgui;

public class QueryImage {
    private static Log LOG = LogFactory.getLog(QueryImage.class);
    public static class Map extends MapReduceBase implements
            Mapper<Text, KeypointDescriptorWritable, DoubleWritable, Text> {
        @Override
        public void configure(JobConf job) {
            Path[] paths = Utils.printDistributedCache(job);

            Utils.printFilesInCurrentDir();
            for (Path p : paths) {
                if (p.toString().endsWith(".so")) {
                    LOG.info("Loading " + p.toString());
                    System.load(p.toString());
                }
            }
        }

        @Override
        public void map(Text key, KeypointDescriptorWritable value,
                OutputCollector<DoubleWritable, Text> context, Reporter reporter)
                throws IOException {
        	File qi = new File("./queryImage.png");
        	LOG.info("File exists: " + qi.exists());
            Mat img = Highgui.imread(qi.getAbsolutePath());

            // extract key points
            FeatureDetector detector = FeatureDetector
                    .create(FeatureDetector.PYRAMID_SIFT);
            MatOfKeyPoint keypoints = new MatOfKeyPoint();
            detector.detect(img, keypoints);

            // extract SIFT descriptors at key points
            DescriptorExtractor extractor = DescriptorExtractor
                    .create(DescriptorExtractor.SIFT);
            Mat descriptors = new Mat();
            extractor.compute(img, keypoints, descriptors);
            
            // Deserialize KeypointDescriptorWritable
            // need a way to deserialize from bytes of MatOfKeyPoint
            // and Mat of descriptors
            Mat candidateDescriptors = value.getDescriptors();
      
            
            DescriptorMatcher matcher = DescriptorMatcher
                    .create(DescriptorMatcher.FLANNBASED);
            MatOfDMatch matches = new MatOfDMatch();
            matcher.match(descriptors, candidateDescriptors, matches);
            
            if (matches.empty()) {
            	LOG.error("Unable to find any matches. Descriptors size:" + descriptors.rows() );
            	return;
            }
            float totalDistance = 0;
            for (DMatch mtch : matches.toArray()) {
                totalDistance += mtch.distance;
            }
            context.collect(new DoubleWritable(totalDistance), key);
        }
    }

    public static class Reduce extends MapReduceBase implements
            Reducer<DoubleWritable, Text, DoubleWritable, Text> {

        @Override
        public void reduce(DoubleWritable distance, Iterator<Text> imageNames,
                OutputCollector<DoubleWritable, Text> output, Reporter reporter)
                throws IOException {
            // Assuming single Reducer
            output.collect(distance, imageNames.next());
            // may have to sort the output file
        }

    }

    /**
     * @param args
     * @throws IOException
     * @throws URISyntaxException
     */
    public static void main(String[] args) throws IOException,
            URISyntaxException {
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args)
                .getRemainingArgs();
        if (otherArgs.length < 2) {
            System.err
                    .println("Usage: bin/hadoop jar /home/vikasdhi/workspace/query-image.jar /output/ queryImage.png");
            System.exit(2);
        }

        JobConf jobconf = new JobConf(ImagesToSIFTDatabase.class);
        jobconf.setJobName("QueryVideo");

        // copy image to HDFS
        FileSystem hdfs = FileSystem.get(conf);
        hdfs.copyFromLocalFile(
                false,
                true,
                new Path(otherArgs[1]),
                new Path(
                        "hdfs://localhost:9000/distributables/queryImage.png"));

        // Opencv needs native library. Put the native shared library in
        // distributed cache and create a Symlink in the current working
        // directory
        Utils.distributeDirectory(jobconf, new Path(
                "hdfs://localhost:9000/libraries/"));
        Utils.distributeDirectory(jobconf, new Path(
                "hdfs://localhost:9000/distributables/"));

        jobconf.setOutputKeyClass(DoubleWritable.class);
        jobconf.setOutputValueClass(Text.class);

        jobconf.setMapperClass(Map.class);

        jobconf.setInputFormat(SequenceFileInputFormat.class);
        jobconf.setOutputFormat(TextOutputFormat.class);

        FileInputFormat
                .setInputPaths(jobconf, new Path(Utils.NAME2DESCRIPTORS));
        FileOutputFormat.setOutputPath(jobconf, new Path(otherArgs[0]));

        JobClient.runJob(jobconf);
    }

}
