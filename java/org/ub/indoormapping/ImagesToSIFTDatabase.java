package org.ub.indoormapping;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.SequenceFileOutputFormat;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;

/* 
 * Motivation: Images are too small for Hadoop. We need to consolidate 
 * small images into big binary Sequence files.
 * 
 * Expected time of query should be around 20 seconds
 *   http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=6182469
 *   
 * Can we make it faster by running map process as local server? Another 
 * approach is to send a batch of images to be queried. 
 * 
 * Status: This map node currently fails with Heapspace error. An alternative 
 * solution is to user tar-to-seq.jar from [1] to conver tar ball to sequence 
 * files and user this node to convert it to a name to descripter sequence file.
 * 
 * [1] http://stuartsierra.com/2008/04/24/a-million-little-files  
 * 
 * Credits:
 *  http://stackoverflow.com/questions/8752772/hadoop-how-to-access-many-photo-images-to-be-processed-by-map-reduce
 *  http://stackoverflow.com/questions/8754154/hadoop-example-process-to-generating-a-sequencefile-with-image-binaries-to-be-p
 *  http://eldadlevy.wordpress.com/2011/02/05/hadoop-binary-files-processing-entroduced-by-image-duplicates-finder/
 *  https://github.com/RyanBalfanz/Babar/blob/master/src/znaflab/hadoop/Babar.java
 */
/**
 * Converts Images into a Hadoop SequenceFile with image name as keys and  image 
 * binary data as values.
 * 
 * @author Vikas Dhiman
 */
public class ImagesToSIFTDatabase {

	private static Log LOG = LogFactory.getLog(ImagesToSIFTDatabase.class);
	public static class Map extends MapReduceBase implements 
    Mapper<LongWritable, Text, Text, KeypointDescriptorWritable> 
    {
    	private static final String IMAGE_DIR="/input/indoormapping/";
    	
    	@Override
    	public void configure(JobConf job) {
            Path[] paths = Utils.printDistributedCache(job);
            
            Utils.printFilesInCurrentDir();
            for (Path p : paths) {
            	if (p.toString().endsWith(".so")) {
            		LOG.info("Loading " + p.toString());
            		System.load(p.toString());
            	}
            }

    	}

		@Override
    	public void map(LongWritable key, Text value, 
    			OutputCollector<Text, KeypointDescriptorWritable> context, Reporter reporter
    			) throws IOException {
            
            String line = value.toString();
            
            // Parse filename from the line. The format of line is :
            // <filename>; (<float>;){16}
            // That is filename followed by 16 floats and each followed by a 
            // ";". The floats together describe the transformation matrix of
            // the image location.
            String[] elements = line.split(";");
            String uri = IMAGE_DIR + elements[0];
            
            // read image from HDFS uri
            Mat img = Utils.imreadFromHDFS(uri);
            
            // extract key points
            FeatureDetector detector = 
            		FeatureDetector.create(FeatureDetector.SIFT);
            MatOfKeyPoint keypoints = new MatOfKeyPoint(); 
            detector.detect(img, keypoints);
            
            // extract SIFT descriptors at key points
            DescriptorExtractor extractor =
            		DescriptorExtractor.create(DescriptorExtractor.SIFT);
            Mat descriptors = new Mat();
            extractor.compute(img, keypoints, descriptors);
            
            System.out.println("Type:" + descriptors.type());
            // Don't ask me why this works :)
            if (descriptors.type() != CvType.CV_32FC1)
            	return;

            
            // TODO: Probably serialize the DescriptorMatcher instead
            // Serialize key points
            KeypointDescriptorWritable kdw = new KeypointDescriptorWritable();
            kdw.setDescriptors(descriptors);
            kdw.setKeypoints(keypoints);
                        
            context.collect(new Text(uri), kdw);
        }

    }
	
	public static void main(String[] args) throws Exception {
		long heapMaxSize = Runtime.getRuntime().maxMemory();
		System.out.println("Max heap size: " + heapMaxSize);
		
		Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        if (otherArgs.length < 2) {
                System.err.println("Usage: bin/hadoop jar /home/vikasdhi/workspace/images2sift.jar /input/indoormapping/info ");
                System.exit(2);
        }
        JobConf jobconf = new JobConf(ImagesToSIFTDatabase.class);
        jobconf.setJobName("ImagesToSIFTDatabase");
        
        // opencv needs native library. Put the native shared library in 
        // distributed cache and create a symlink in the current working directory
        Utils.distributeDirectory(jobconf, 
        		new Path("hdfs://localhost:9000/libraries/"));
        
        jobconf.setOutputKeyClass(Text.class);
        jobconf.setOutputValueClass(KeypointDescriptorWritable.class);
        
        jobconf.setMapperClass(Map.class);
        jobconf.setNumReduceTasks(0);
        
        jobconf.setInputFormat(TextInputFormat.class);
        jobconf.setOutputFormat(SequenceFileOutputFormat.class);
        
        FileInputFormat.setInputPaths(jobconf, new Path(otherArgs[0]));
        FileOutputFormat.setOutputPath(jobconf, 
        		new Path(Utils.NAME2DESCRIPTORS));
        
        JobClient.runJob(jobconf);
    }

}
