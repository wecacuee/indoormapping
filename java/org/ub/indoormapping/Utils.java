package org.ub.indoormapping;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.mapred.JobConf;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

public class Utils {
	private static Log LOG = LogFactory.getLog(Utils.class);
	public static void distributeDirectory(JobConf jobconf, Path directory)
			throws IOException, URISyntaxException {
		 
        // opencv needs native library. Put the native shared library in 
        // distributed cache and create a symlink in the current working directory
		DistributedCache.createSymlink(jobconf);
        FileSystem fs = FileSystem.get(new Configuration());
        FileStatus[] status = fs.listStatus(directory);
        for (FileStatus stat : status) {
        	Path pth = stat.getPath();
        	DistributedCache.addCacheFile(
    			new URI(pth.toString() + "#" + pth.getName()),
                jobconf);
        }
	}

	public static final String NAME2DESCRIPTORS = "/output/name2descriptors";

	/*
	 * Source: 
	 * 	http://stackoverflow.com/questions/80476/how-to-concatenate-two-arrays-in-java
	 */
	public static byte[] concatenate (byte[] A, byte[] B) {
	    int aLen = A.length;
	    int bLen = B.length;
	
	    byte[] C = new byte[aLen + bLen];
	    System.arraycopy(A, 0, C, 0, aLen);
	    System.arraycopy(B, 0, C, aLen, bLen);
	
	    return C;
	}

	public static Path[] printDistributedCache(JobConf job) {
		LOG.info("java.library.path:" + System.getProperty("java.library.path"));
	    Path[] paths = null;
	    try {
	    	paths = DistributedCache.getLocalCacheFiles(job);
	    	StringBuffer message = new StringBuffer("local cache files:");
			for (Path p : paths) {
				message.append(p);
				message.append(";\n\t");
			}
			LOG.info(message.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return paths;
	}

	public static void printFilesInCurrentDir() {
		// Directory path here
	    String path = "."; 
	
	    String files;
	    File folder = new File(path);
	    File[] listOfFiles = folder.listFiles();
	    StringBuffer message = new StringBuffer(
	    		"CWD: " + folder.getAbsolutePath() + "; Files:");
	
	    for (int i = 0; i < listOfFiles.length; i++) 
	    {
	
	    	if (listOfFiles[i].isFile()) 
	    	{
	    		files = listOfFiles[i].getName();
	    		message.append(files);
	    		message.append(";\n\t");
	    	}
	    }
	    LOG.info(message.toString());
	}
	
	public static Mat imreadFromHDFS(String uri) throws IOException,
			FileNotFoundException {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(URI.create(uri), conf);
		FSDataInputStream in = null;

		// Opencv imread only accepts filenames. So we have to write this
		// inputstream to a temp file
		File temp = File.createTempFile("img", "png");
		LOG.error("OpenCV file:" + temp.getPath());
		try {
			in = fs.open(new Path(uri));
			IOUtils.copyBytes(in, new FileOutputStream(temp), conf);
		} finally {
			IOUtils.closeStream(in);
		}

		// read the image
		String fname = temp.getAbsolutePath();
		Mat img = Highgui.imread(fname, Highgui.IMREAD_GRAYSCALE); // read as
																	// grayscale
		temp.delete();
		return img;
	}
}
