package org.ub.indoormapping;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;
import org.opencv.highgui.Highgui;

public class KeypointDescriptorWritable implements Writable {
	private MatOfKeyPoint keypoints_;
	private Mat descriptors_;
	
	public MatOfKeyPoint getKeypoints() {
		return keypoints_;
	}

	public void setKeypoints(MatOfKeyPoint keypoints) {
		keypoints_ = keypoints;
	}

	public Mat getDescriptors() {
		return descriptors_;
	}

	public void setDescriptors(Mat descriptors) {
		descriptors_ = descriptors;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		IntWritable rows = new IntWritable();
		rows.readFields(in);
		
		IntWritable cols = new IntWritable();
		cols.readFields(in);
		
		keypoints_ = new MatOfKeyPoint();
		keypoints_.alloc(rows.get());
		int bufferLength = (int)(keypoints_.total() * keypoints_.channels() * 4);
		byte[] keypointsBuffer = new byte[bufferLength];
		in.readFully(keypointsBuffer, 0, bufferLength);
		
		FloatBuffer kpfloatbuf = ByteBuffer.wrap(keypointsBuffer).asFloatBuffer();
		float[] kpfloats = new float[bufferLength / 4];
		kpfloatbuf.get(kpfloats);
		keypoints_.put(0, 0, kpfloats);
		
		IntWritable drows = new IntWritable();
		drows.readFields(in);
		IntWritable dcols = new IntWritable();
		dcols.readFields(in);
		IntWritable dtype = new IntWritable();
		dtype.readFields(in);
		
		descriptors_ = new Mat(drows.get(), dcols.get(), dtype.get());
		int dbufflen = (int)(descriptors_.total() * descriptors_.channels() * 4);
		byte[] descrsBuffer = new byte[dbufflen];
		in.readFully(descrsBuffer, 0, dbufflen);
		
		FloatBuffer descrsFloatBuf = ByteBuffer.wrap(descrsBuffer).asFloatBuffer();
		float[] descrsFloats = new float[dbufflen / 4];
		descrsFloatBuf.get(descrsFloats);
		descriptors_.put(0, 0, descrsFloats);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(keypoints_.rows());
		out.writeInt(keypoints_.cols());
		float[] keypointsBuffer = 
        		new float[(int)(keypoints_.total() * keypoints_.channels())];
		keypoints_.get(0, 0, keypointsBuffer);
		ByteBuffer kpbyteBuf = ByteBuffer.allocate(keypointsBuffer.length * 4);
		FloatBuffer kpfloatBuf = kpbyteBuf.asFloatBuffer();
		kpfloatBuf.put(keypointsBuffer);
		out.write(kpbyteBuf.array());
		
		out.writeInt(descriptors_.rows());
		out.writeInt(descriptors_.cols());
		out.writeInt(descriptors_.type());
		float[] descrsBuffer = 
        		new float[(int)(descriptors_.total() * descriptors_.channels())];
		descriptors_.get(0, 0, descrsBuffer);
		ByteBuffer byteBuf = ByteBuffer.allocate(descrsBuffer.length * 4);
		FloatBuffer floatBuf = byteBuf.asFloatBuffer();
		floatBuf.put(descrsBuffer);
		out.write(byteBuf.array());
	}
	
	public static void main(String[] args) throws IOException {
		System.load("/usr/local/share/OpenCV/java/libopencv_java247.so");
		System.out.println("arg[0] : " + args[0]);
		Mat img = Highgui.imread(args[0]);
		// test serialization deserialization.
		// extract key points
        FeatureDetector detector = 
        		FeatureDetector.create(FeatureDetector.SIFT);
        MatOfKeyPoint keypoints = new MatOfKeyPoint(); 
        detector.detect(img, keypoints);
        
        // extract SIFT descriptors at key points
        DescriptorExtractor extractor =
        		DescriptorExtractor.create(DescriptorExtractor.SIFT);
        Mat descriptors = new Mat();
        extractor.compute(img, keypoints, descriptors);
        
        KeypointDescriptorWritable kdw = new KeypointDescriptorWritable();
        kdw.setDescriptors(descriptors);
        kdw.setKeypoints(keypoints);
        
        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
        DataOutputStream dataOut = new DataOutputStream(byteout);
        kdw.write(dataOut);
        
        ByteArrayInputStream bytein = 
        		new ByteArrayInputStream(byteout.toByteArray());
        
        KeypointDescriptorWritable kdwread = new KeypointDescriptorWritable();
        kdwread.readFields(new DataInputStream(bytein));
        
        assert descriptors.equals(kdwread.getDescriptors());
	}

}
